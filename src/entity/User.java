package entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UZYTKOWNICY")
public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8709331993504038951L;
	@Id 
	@GeneratedValue
	@Column(name = "id")
	private int id;
	@Column(name = "imie")
	private String imie;
	@Column(name = "nazwisko")
	private String nazwisko;
	@Column(name = "login")
	private String login;
	@Column(name = "haslo")
	private String haslo;
	@Column(name = "email")
	private String email;
	@Column(name = "data_zatrudnienia")
	private Date dataZatrudnienia;
	@Column(name = "data_zwolnienia")
	private Date dataZwolnienia;
	@Column(name = "stanowisko")
	private String stanowisko;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getHaslo() {
		return haslo;
	}
	public void setHaslo(String haslo) {
		this.haslo = haslo;
	}
	public Date getDataZatrudnienia() {
		return dataZatrudnienia;
	}
	public void setDataZatrudnienia(Date dataZatrudnienia) {
		this.dataZatrudnienia = dataZatrudnienia;
	}
	public Date getDataZwolnienia() {
		return dataZwolnienia;
	}
	public void setDataZwolnienia(Date dataZwolnienia) {
		this.dataZwolnienia = dataZwolnienia;
	}
	public String getStanowisko() {
		return stanowisko;
	}
	public void setStanowisko(String stanowisko) {
		this.stanowisko = stanowisko;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
