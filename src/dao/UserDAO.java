package dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entity.User;



@Stateless
public class UserDAO implements UserDAOInterface {

	 @PersistenceContext
	    private EntityManager entityManager;
	 
	

	@Override
	public void zapiszUsera(User user) {
		entityManager.merge(user);
		
	}
	
	
	
	 public EntityManager getEntityManager() {
			return entityManager;
		}


		public void setEntityManager(EntityManager entityManager) {
			this.entityManager = entityManager;
		}

	
}
