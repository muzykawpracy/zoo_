package co;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import dao.UserDAOInterface;
import entity.User;



@ManagedBean
@ViewScoped
public class DodawanieUzytkownikaMB implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1102086065459898974L;

	@Inject
	UserDAOInterface userDAO;
	
	@PostConstruct
	public void init(){
		System.out.println("xx");
	}
	
	private User nowyUzytkownik = new User();

	public User getNowyUzytkownik() {
		return nowyUzytkownik;
	}

	public void setNowyUzytkownik(User nowyUzytkownik) {
		this.nowyUzytkownik = nowyUzytkownik;
	}
	
	public void zapiszUzytkownika(){
		nowyUzytkownik.setHaslo("aaaaa");
		userDAO.zapiszUsera(nowyUzytkownik);
	}
	
	public String dodaj(){
		return "dodajPracownika?faces-redirect=true";
	}

	public UserDAOInterface getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAOInterface userDAO) {
		this.userDAO = userDAO;
	}
	
}
