package co;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import entity.User;



@ManagedBean
@SessionScoped
public class LoggingMB {
	
	private User loggedInUser;
	private String userName;
	private String password;
	
	
	public String login(){
		
		return "index";
	}
	
	
	public User getLoggedInUser() {
		return loggedInUser;
	}
	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
